var twilio = require('twilio'),
    express = require('express');

var route = express.Router();

route.get('/mainMenuIT',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'mainMenuActionIT'},function(){
        this.say('Per la conformità, premere 1',{
            voice:'alice',
            language:'it-IT'
        });
        this.say('Per la finanza, premere 2',{
            voice:'alice',
            language:'it-IT'
        });
        this.say('Per problemi tecnici, premere 3',{
            voice:'alice',
            language:'it-IT'
        });
        this.say('Per presentare un reclamo, premere 4',{
            voice:'alice',
            language:'it-IT'
        });
        this.say('Per raggiungere la sala contrattazioni, premere 5',{
            voice:'alice',
            language:'it-IT'
        });
        this.say('Per raggiungere il team di vendita, premere 6',{
            voice:'alice',
            language:'it-IT'
        });
        this.say('Per raggiungere il tuo account executive, premere 7',{
            voice:'alice',
            language:'it-IT'
        });
        this.say('Per tutte le altre questioni, premere 8',{
            voice:'alice',
            language:'it-IT'
        });
    })
    res.type('xml')
    res.end(resp.toString())
})

route.get('/mainMenuActionIT',function(req, res){
    var resp = new twilio.TwimlResponse();
    var digits = req.query.Digits;
    if (digits == 1){
        resp.redirect({method:'get'},'/complianceIT');
    }
    if (digits == 2){
        resp.redirect({method:'get'},'/financeIT');
    }
    if(digits == 3){
        resp.redirect({method:'get'},'/technicalIssuesIT');
    }
    if(digits == 4){
        resp.redirect({method:'get'},'/complaintIT');
    }
    if(digits == 5){
        resp.redirect({method:'get'},'/dealingRoomIT');
    }
    if(digits == 6){
        resp.redirect({method:'get'},'/salesIT');
    }
    if(digits == 7){
        resp.redirect({method:'get'},'/accountExecutiveIT');
    }
    if(digits == 8){
        resp.redirect({method:'get'},'/otherIT');
    }
    res.type('xml')
    res.end(resp.toString())
})
route.get('/complianceIT',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'/complianceITAction'},function(){
        this.say('Per ottenere il vostro account verificato, premere 1',{
            voice:'alice',
            language:'it-IT'
        });
        this.say("Per ottenere un aggiornamento sul proprio stato di verifica dell'account, premere 2",{
            voice:'alice',
            language:'it-IT'
        });
        this.say('Per tornare al menu principale, premere 9',{
            voice:'alice',
            language:'it-IT'
        });
    });
    res.type('xml');
    res.end(resp.toString());
})
route.get('/complianceITAction',function(req, res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if(digit == 9){
        resp.redirect({method:'get'},'/mainMenuIT')
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/financeIT',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get', action:'financeITAction'},function(){
        this.say('Per depositare fondi sul tuo conto, premere 1',{
            voice:'alice',
            language:'it-IT'
        });
        this.say('Per richiedere un prelievo, premere 2',{
            voice:'alice',
            language:'it-IT'
        });
        this.say('Per controllare lo stato del ritiro, premere 3',{
            voice:'alice',
            language:'it-IT'
        })
        this.say('Per tornare al menu principale, premere 9',{
            voice:'alice',
            language:'it-IT'
        })
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/financeITAction',function(req, res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if(digit == 9){
        resp.redirect({method:'get'},'/mainMenuIT');
    }
    res.type('xml')
    res.end(resp.toString())
})
route.get('/technicalIssuesIT',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get', action:'technicalIssuesITAction'},function(){
        this.say('Per una questioni generali, premere 1',{
            voice:'alice',
            language:'it-IT'
        });
        this.say('Per problemi tecnici, premere 2',{
            voice:'alice',
            language:'it-IT'
        })
        this.say('Per problemi con la piattaforma, premere 3',{
            voice:'alice',
            language:'it-IT'
        })
        this.say('Per tornare al menu principale, premere 9',{
            voice:'alice',
            language:'it-IT'
        })
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/technicalIssuesITAction',function(req, res){
    var resp = new twilio.TwimlResponse();
    var digit = req.query.Digits;
    if(digit == 9){
        resp.redirect({method:'get'},'/mainMenuIT');
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/complaintIT',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get', action:'complaintITAction'},function(){
        this.say('Sei denunce sono molto importanti per noi, prego tenere mentre trasferiamo la chiamata al nostro ufficio esecutivo',{
            voice:'alice',
            language:'it-IT'
        });
        this.say('Per tornare al menu principale, premere 9',{
            voice:'alice',
            language:'it-IT'
        })
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/complaintITAction',function(req, res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if(digit == 9){
        resp.redirect({method:'get'},'mainMenuIT');
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/dealingRoomIT',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'dealingRoomITAction'},function(){
        this.say('Si prega di lasciare un messaggio e vi risponderemo entro 24 ore di affari',{
            voice:'alice',
            language:'it-IT'
        });
        this.say('Per tornare al menu principale, premere 9',{
            voice:'alice',
            language:'it-IT'
        })
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/dealingRoomITAction',function(req,res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if (digit == 9){
        resp.redirect({method:'get'},'/mainMenuIT')
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/salesIT',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'salesITAction'},function(){
        this.say('Per parlare con un account manager, premere 1',{
            voice:'alice',
            language:'it-IT'
        })
        this.say('Per parlare con un supervisore, premere 2',{
            voice:'alice',
            language:'it-IT'
        })
        this.say('Per saperne di più sui nostri prodotti di trading, por favor premere 3',{
            voice:'alice',
            language:'it-IT'
        });
        this.say('Per tornare al menu principale premere 9',{
            voice:'alice',
            language:'it-IT'
        });
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/salesITAction',function(req,res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if (digit == 9){
        resp.redirect({method:'get'},'/mainMenuIT')
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/accountExecutiveIT',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'accountExecutiveITAction'},function() {
        this.say('Si prega di lasciare un messaggio con il proprio nome e il nome del vostro account manager, e sarà seguito entro 24 ore lavorative.',{
            voice:'alice',
            language:'it-IT'
        })
        this.say('Per lasciare un messaggio, premere 1',{
            voice:'alice',
            language:'it-IT'
        })
        this.say('Per parlare con un account executive a caso, premere 2',{
            voice:'alice',
            language:'it-IT'
        });
        this.say('Per il servizio al cliente, premere 3',{
            voice:'alice',
            language:'it-IT'
        });
        this.say('Per tornare al menu principale, premere 9',{
            voice:'alice',
            language:'it-IT'
        })
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/accountExecutiveITAction',function(req, res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if (digit == 9){
        resp.redirect({method:'get'},'/mainMenuIT')
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/otherIT',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'otherITAction'},function(){
        this.say('Si prega di lasciare un messaggio dopo il segnale acustico e qualcuno seguirà con voi entro 24 ore lavorative.',{
            voice:'alice',
            language:'it-IT'
        })
        this.say('Per tornare al menu principale, premere 9',{
            voice:'alice',
            language:'it-IT'
        });
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/otherITAction',function(req, res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if (digit == 9){
        resp.redirect({method:'get'},'/mainMenuIT')
    }
    res.type('xml')
    res.end(resp.toString());
})

module.exports = route;