var twilio = require('twilio'),
    express = require('express');

var route = express.Router();

route.get('/mainMenuRU',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'mainMenuActionRU'},function(){
        this.say('Dlya sootvetstviya , nazhmite 1',{
            voice:'alice',
            language: 'ru-RU'
        });
        this.say('Dlya finansov, nazhmite 2',{
            voice:'alice',
            language: 'ru-RU'
        });
        this.say('Po tekhnicheskim voprosam , nazhmite 3',{
            voice:'alice',
            language: 'ru-RU'
        });
        this.say("Chtoby podat' zhalobu , nazhmite 4",{
            voice:'alice',
            language: 'ru-RU'
        });
        this.say("Chtoby dobrat'sya do dilingovogo zala , nazhmite 5",{
            voice:'alice',
            language: 'ru-RU'
        });
        this.say("Chtoby dobrat'sya do otdela prodazh , nazhmite 6",{
            voice:'alice',
            language: 'ru-RU'
        });
        this.say('Dlya dostizheniya svoyemu menedzheru uchetnykh zapisey , nazhmite 7',{
            voice:'alice',
            language: 'ru-RU'
        });
        this.say('Dlya vsekh drugikh voprosov , nazhmite 8',{
            voice:'alice',
            language: 'ru-RU'
        });
    })
    res.type('xml')
    res.end(resp.toString())
})

route.get('/mainMenuActionRU',function(req, res){
    var resp = new twilio.TwimlResponse();
    var digits = req.query.Digits;
    if (digits == 1){
        resp.redirect({method:'get'},'/complianceRU');
    }
    if (digits == 2){
        resp.redirect({method:'get'},'/financeRU');
    }
    if(digits == 3){
        resp.redirect({method:'get'},'/technicalIssuesRU');
    }
    if(digits == 4){
        resp.redirect({method:'get'},'/complaintRU');
    }
    if(digits == 5){
        resp.redirect({method:'get'},'/dealingRoomRU');
    }
    if(digits == 6){
        resp.redirect({method:'get'},'/salesRU');
    }
    if(digits == 7){
        resp.redirect({method:'get'},'/accountExecutiveRU');
    }
    if(digits == 8){
        resp.redirect({method:'get'},'/otherRU');
    }
    res.type('xml')
    res.end(resp.toString())
})
route.get('/complianceRU',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'/complianceRUAction'},function(){
        this.say("Dlya polucheniya svoyego akkaunta proverit', nazhmite 1",{
            voice:'alice',
            language: 'ru-RU'
        });
        this.say("Chtoby poluchit' obnovlennuyu informatsiyu o vashey uchetnoy zapisi sostoyaniya proverki , nazhmite 2",{
            voice:'alice',
            language: 'ru-RU'
        });
        this.say('Dlya vozvrata v glavnoye menyu , nazhmite 9',{
            voice:'alice',
            language: 'ru-RU'
        });
    });
    res.type('xml');
    res.end(resp.toString());
})
route.get('/complianceRUAction',function(req, res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if(digit == 9){
        resp.redirect({method:'get'},'/mainMenuRU')
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/financeRU',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get', action:'financeRUAction'},function(){
        this.say("Chtoby vnesti den'gi na svoy schet , nazhmite 1",{
            voice:'alice',
            language: 'ru-RU'
        });
        this.say("Chtoby poluchit' vyvod , nazhmite 2",{
            voice:'alice',
            language: 'ru-RU'
        });
        this.say('Dlya proverki sostoyaniya vyvoda , nazhmite 3',{
            voice:'alice',
            language: 'ru-RU'
        })
        this.say('Dlya vozvrata v glavnoye menyu , nazhmite 9',{
            voice:'alice',
            language: 'ru-RU'
        })
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/financeRUAction',function(req, res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if(digit == 9){
        resp.redirect({method:'get'},'/mainMenuRU');
    }
    res.type('xml')
    res.end(resp.toString())
})
route.get('/technicalIssuesRU',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get', action:'technicalIssuesRUAction'},function(){
        this.say('Dlya obshchego dela, nazhmite 1',{
            voice:'alice',
            language: 'ru-RU'
        });
        this.say('Dlya tekhnicheskikh problem , nazhmite 2',{
            language: 'ru-RU'
        })
        this.say('Dlya voprosov s platformy , nazhmite 3',{
            voice:'alice',
            language: 'ru-RU'
        })
        this.say('Dlya vozvrata v glavnoye menyu , nazhmite 9',{
            voice:'alice',
            language: 'ru-RU'
        })
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/technicalIssuesRUAction',function(req, res){
    var resp = new twilio.TwimlResponse();
    var digit = req.query.Digits;
    if(digit == 9){
        resp.redirect({method:'get'},'/mainMenuRU');
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/complaintRU',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get', action:'complaintRUAction'},function(){
        this.say("Vy zhaloby ochen' vazhny dlya nas, pozhaluysta, ne otpuskaya yeye, kak my perenesem vash vyzov nashey ispolnitel'nogo organa",{
            voice:'alice',
            language: 'ru-RU'
        });
        this.say('Dlya vozvrata v glavnoye menyu , nazhmite 9',{
            voice:'alice',
            language: 'ru-RU'
        })
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/complaintRUAction',function(req, res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if(digit == 9){
        resp.redirect({method:'get'},'mainMenuRU');
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/dealingRoomRU',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'dealingRoomRUAction'},function(){
        this.say("Pozhaluysta, ostav'te nam soobshcheniye i my svyazhemsya s Vami v techeniye 24 rabochikh chasov",{
            voice:'alice',
            language: 'ru-RU'
        });
        this.say('Dlya vozvrata v glavnoye menyu , nazhmite 9',{
            voice:'alice',
            language: 'ru-RU'
        })
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/dealingRoomRUAction',function(req,res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if (digit == 9){
        resp.redirect({method:'get'},'/mainMenuRU')
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/salesRU',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'salesRUAction'},function(){
        this.say("Chtoby pogovorit' s menedzherom scheta , nazhmite 1",{
            voice:'alice',
            language: 'ru-RU'
        })
        this.say("Chtoby svyazat'sya s rukovoditelem, nazhmite 2",{
            voice:'alice',
            language: 'ru-RU'
        })
        this.say("Chtoby uznat' bol'she o nashikh torgovykh produktov, POR pol'zu nazhmite 3",{
            voice:'alice',
            language: 'ru-RU'
        });
        this.say('Dlya vozvrata v glavnoye menyu , nazhmite 9',{
            voice:'alice',
            language: 'ru-RU'
        });
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/salesRUAction',function(req,res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if (digit == 9){
        resp.redirect({method:'get'},'/mainMenuRU')
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/accountExecutiveRU',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'accountExecutiveRUAction'},function() {
        this.say("Pozhaluysta, ostav'te nam soobshcheniye s Vashim imenem i imya menedzhera vashego scheta , i vy budet prodolzhena v techeniye 24 rabochikh chasov.",{
            voice:'alice',
            language: 'ru-RU'
        })
        this.say("Chtoby ostavit' soobshcheniye , nazhmite 1",{
            voice:'alice',
            language: 'ru-RU'
        });
        this.say("Chtoby svyazat'sya s ispolnitel'noy proizvol'nyy schet , nazhmite 2",{
            voice:'alice',
            language: 'ru-RU'
        });
        this.say('Dlya obsluzhivaniya kliyentov, nazhmite 3',{
            voice:'alice',
            language: 'ru-RU'
        });
        this.say('Dlya vozvrata v glavnoye menyu , nazhmite 9',{
            voice:'alice',
            language: 'ru-RU'
        })
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/accountExecutiveRUAction',function(req, res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if (digit == 9){
        resp.redirect({method:'get'},'/mainMenuRU')
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/otherRU',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'otherRUAction'},function(){
        this.say("Pozhaluysta, ostav'te soobshcheniye posle signala , i kto budet sledovat' s vami v techeniye 24 rabochikh chasov.",{
            voice:'alice',
            language: 'ru-RU'
        })
        this.say('Dlya vozvrata v glavnoye menyu , nazhmite 9',{
            voice:'alice',
            language: 'ru-RU'
        });
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/otherRUAction',function(req, res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if (digit == 9){
        resp.redirect({method:'get'},'/mainMenuRU')
    }
    res.type('xml')
    res.end(resp.toString());
})

module.exports = route;