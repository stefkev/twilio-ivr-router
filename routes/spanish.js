var twilio = require('twilio'),
    express = require('express');

var route = express.Router();

route.get('/mainMenuES',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'mainMenuESAction'},function(){
        this.say('Para su cumplimiento, pulse 1',{
            voice:'alice',
            language:'es-ES'
        });
        this.say('Para finanzas, pulse 2',{
            voice:'alice',
            language:'es-ES'
        });
        this.say('Por cuestiones técnicas, pulse 3',{
            voice:'alice',
            language:'es-ES'
        });
        this.say('Para presentar una queja, pulse 4',{
            voice:'alice',
            language:'es-ES'
        });
        this.say('Para llegar a la sala de operaciones, pulse 5',{
            voice:'alice',
            language:'es-ES'
        });
        this.say('Para llegar al equipo de ventas, presione 6',{
            voice:'alice',
            language:'es-ES'
        });
        this.say('Para todos los demás asuntos, presione 8',{
            voice:'alice',
            language:'es-ES'
        });
    })
    res.type('xml')
    res.end(resp.toString())
})

route.get('/mainMenuESAction',function(req, res){
    var resp = new twilio.TwimlResponse();
    var digits = req.query.Digits;
    if (digits == 1){
        resp.redirect({method:'get'},'/complianceES');
    }
    if (digits == 2){
        resp.redirect({method:'get'},'/financeES');
    }
    if(digits == 3){
        resp.redirect({method:'get'},'/technicalIssuesES');
    }
    if(digits == 4){
        resp.redirect({method:'get'},'/complaintES');
    }
    if(digits == 5){
        resp.redirect({method:'get'},'/dealingRoomES');
    }
    if(digits == 6){
        resp.redirect({method:'get'},'/salesES');
    }
    if(digits == 7){
        resp.redirect({method:'get'},'/accountExecutiveES');
    }
    if(digits == 8){
        resp.redirect({method:'get'},'/otherES');
    }
    res.type('xml')
    res.end(resp.toString())
})
route.get('/complianceES',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'/complianceESAction'},function(){
        this.say('Para obtener su cuenta verificado, pulse 1',{
            voice:'alice',
            language:'es-ES'
        });
        this.say('Para obtener información actualizada sobre su estado de verificación de la cuenta, pulse 2',{
            voice:'alice',
            language:'es-ES'
        });
        this.say('Para volver al menú principal, presione 9',{
            voice:'alice',
            language:'es-ES'
        });
    });
    res.type('xml');
    res.end(resp.toString());
})
route.get('/complianceESAction',function(req, res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if(digit == 9){
        resp.redirect({method:'get'},'/mainMenuES')
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/financeES',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get', action:'financeESAction'},function(){
        this.say('Para depositar fondos en su cuenta, pulse 1',{
            voice:'alice',
            language:'es-ES'
        });
        this.say('Para solicitar un retiro, pulse 2',{
            voice:'alice',
            language:'es-ES'
        });
        this.say('Para comprobar el estado de la retirada, presione 3',{
            voice:'alice',
            language:'es-ES'
        })
            this.say('Para volver al menú principal, presione 9',{
                voice:'alice',
                language:'es-ES'
            })
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/financeESAction',function(req, res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if(digit == 9){
        resp.redirect({method:'get'},'/mainMenuES');
    }
    res.type('xml')
    res.end(resp.toString())
})
route.get('/technicalIssuesES',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get', action:'technicalIssuesESAction'},function(){
        this.say('Respecto de los asuntos generales, pulse 1',{
            voice:'alice',
            language:'es-ES'
        });
        this.say('Por problemas técnicos, pulse 2',{
            voice:'alice',
            language:'es-ES'
        })
        this.say('Por problemas con la plataforma, presione 3',{
            voice:'alice',
            language:'es-ES'
        })
        this.say('Para volver al menú principal, presione 9',{
            voice:'alice',
            language:'es-ES'
        })
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/technicalIssuesESAction',function(req, res){
    var resp = new twilio.TwimlResponse();
    var digit = req.query.Digits;
    if(digit == 9){
        resp.redirect({method:'get'},'/mainMenuES');
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/complaintES',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get', action:'complaintESAction'},function(){
        this.say('Usted quejas es muy importante para nosotros, por favor espere mientras transferimos su llamada a nuestra oficina ejecutiva',{
            voice:'alice',
            language:'es-ES'
        });
        this.say('Para volver al menú principal, presione 9',{
            voice:'alice',
            language:'es-ES'
        })
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/complaintESAction',function(req, res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if(digit == 9){
        resp.redirect({method:'get'},'mainMenuES');
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/dealingRoomES',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'dealingRoomESAction'},function(){
        this.say('Por favor déjenos un mensaje y nos pondremos en contacto con usted dentro de 24 horas de oficina',{
            voice:'alice',
            language:'es-ES'
        });
        this.say('Para volver al menú principal, presione 9',{
            voice:'alice',
            language:'es-ES'
        })
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/dealingRoomESAction',function(req,res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if (digit == 9){
        resp.redirect({method:'get'},'/mainMenuES')
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/salesES',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'salesESAction'},function(){
        this.say('Para hablar con un gerente de cuenta, pulse 1',{
            voice:'alice',
            language:'es-ES'
        })
        this.say('Para hablar con el supervisor, pulse 2',{
            voice:'alice',
            language:'es-ES'
        })
        this.say('Para saber más sobre nuestros productos comerciales, por favor presione 3',{
            voice:'alice',
            language:'es-ES'
        });
        this.say('To return to the main menu press 9',{
            voice:'alice',
            language:'es-ES'
        });
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/salesESAction',function(req,res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if (digit == 9){
        resp.redirect({method:'get'},'/mainMenuES')
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/accountExecutiveES',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'accountExecutiveESAction'},function() {
        this.say('Por favor déjenos un mensaje con su nombre y el nombre de su ejecutivo de cuenta, y se le seguirá en un plazo de 24 horas hábiles.',{
            voice:'alice',
            language:'es-ES'
        })
        this.say('Para dejar un mensaje, pulse 1',{
            voice:'alice',
            language:'es-ES'
        })
        this.say('Para hablar con un ejecutivo de cuentas al azar, pulse 2',{
            voice:'alice',
            language:'es-ES'
        });
        this.say('Para servicio al cliente, pulse 3',{
            voice:'alice',
            language:'es-ES'
        });
        this.say('Para volver al menú principal, presione 9',{
            voice:'alice',
            language:'es-ES'
        })
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/accountExecutiveESAction',function(req, res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if (digit == 9){
        resp.redirect({method:'get'},'/mainMenuES')
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/otherES',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'otherESAction'},function(){
        this.say('Por favor, deje un mensaje después del tono y alguien se comunicará con usted dentro de 24 horas de oficina.',{
            voice:'alice',
            language:'es-ES'
        })
        this.say('Para volver al menú principal, presione 9',{
            voice:'alice',
            language:'es-ES'
        });
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/otherENAction',function(req, res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if (digit == 9){
        resp.redirect({method:'get'},'/mainMenuES')
    }
    res.type('xml')
    res.end(resp.toString());
})

module.exports = route;