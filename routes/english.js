var twilio = require('twilio'),
    express = require('express');

var route = express.Router();

route.get('/mainMenuEN',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'mainMenuActionEN'},function(){
        this.say('For compliance, press 1',{
            voice:'alice'
        });
        this.say('For finance, press 2',{
            voice:'alice'
        });
        this.say('For technical issues, press 3',{
            voice:'alice'
        });
        this.say('To file a complaint, press 4',{
            voice:'alice'
        });
        this.say('To reach the dealing room, press 5',{
            voice:'alice'
        });
        this.say('To reach the sales team, press 6',{
            voice:'alice'
        });
        this.say('To reach your account executive, press 7',{
            voice:'alice'
        });
        this.say('For all other matters, press 8',{
            voice:'alice'
        });
    })
    res.type('xml')
    res.end(resp.toString())
})

route.get('/mainMenuActionEN',function(req, res){
    var resp = new twilio.TwimlResponse();
    var digits = req.query.Digits;
    if (digits == 1){
        resp.redirect({method:'get'},'/complianceEN');
    }
    if (digits == 2){
        resp.redirect({method:'get'},'/financeEN');
    }
    if(digits == 3){
        resp.redirect({method:'get'},'/technicalIssuesEN');
    }
    if(digits == 4){
        resp.redirect({method:'get'},'/complaintEN');
    }
    if(digits == 5){
        resp.redirect({method:'get'},'/dealingRoomEN');
    }
    if(digits == 6){
        resp.redirect({method:'get'},'/salesEN');
    }
    if(digits == 7){
        resp.redirect({method:'get'},'/accountExecutiveEN');
    }
    if(digits == 8){
        resp.redirect({method:'get'},'/otherEN');
    }
    res.type('xml')
    res.end(resp.toString())
})
route.get('/complianceEN',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'/complianceENAction'},function(){
        this.say('To get your account verified, press 1',{
            voice:'alice'
        });
        this.say('To get an update on your account verification status, press 2',{
            voice:'alice'
        });
        this.say('To return to the main menu, press 9',{
            voice:'alice'
        });
    });
    res.type('xml');
    res.end(resp.toString());
})
route.get('/complianceENAction',function(req, res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if(digit == 9){
        resp.redirect({method:'get'},'/mainMenuEN')
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/financeEN',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get', action:'financeENAction'},function(){
        this.say('To deposit funds to your account, press 1',{
            voice:'alice'
        });
        this.say('To request a withdrawal, press 2',{
            voice:'alice'
        });
        this.say('To check on a status of the withdrawal, press 3',{
            voice:'alice'
        })
        this.say('To return to the main menu, press 9',{
            voice:'alice'
        })
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/financeENAction',function(req, res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if(digit == 9){
        resp.redirect({method:'get'},'/mainMenuEN');
    }
    res.type('xml')
    res.end(resp.toString())
})
route.get('/technicalIssuesEN',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get', action:'technicalIssuesENAction'},function(){
        this.say('For general matters, press 1',{
            voice:'alice'
        });
        this.say('For technical problems, press 2',{
            voice:'alice'
        })
        this.say('For issues with platform, press 3',{
            voice:'alice'
        })
        this.say('To return to the main menu, press 9',{
            voice:'alice'
        })
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/technicalIssuesENAction',function(req, res){
    var resp = new twilio.TwimlResponse();
    var digit = req.query.Digits;
    if(digit == 9){
        resp.redirect({method:'get'},'/mainMenuEN');
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/complaintEN',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get', action:'complaintENAction'},function(){
        this.say('You complaints are very important to us, please hold while we transfer your call to our executive office',{
            voice:'alice'
        });
        this.say('To return to the main menu, press 9',{
            voice:'alice'
        })
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/complaintENAction',function(req, res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if(digit == 9){
        resp.redirect({method:'get'},'mainMenuEN');
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/dealingRoomEN',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'dealingRoomENAction'},function(){
        this.say('Please leave us a message and we will get back to you within 24 business hours',{
            voice:'alice'
        });
        this.say('To return to the main menu, press 9',{
            voice:'alice'
        })
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/dealingRoomENAction',function(req,res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if (digit == 9){
        resp.redirect({method:'get'},'/mainMenuEN')
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/salesEN',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'salesENAction'},function(){
        this.say('To speak to an account manager, press 1',{
            voice:'alice'
        })
        this.say('To speak to supervisor, press 2',{
            voice:'alice'
        })
        this.say('To find out more about our trading products, please press 3',{
            voice:'alice'
        });
        this.say('To return to the main menu press 9',{
            voice:'alice'
        });
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/salesENAction',function(req,res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if (digit == 9){
        resp.redirect({method:'get'},'/mainMenuEN')
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/accountExecutiveEN',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'accountExecutiveENAction'},function() {
        this.say('Please leave us a message with your name and the name of your account manager, and you will be followed up within 24 business hours.',{
            voice:'alice'
        })
        this.say('To leave a message, press 1',{
            voice:'alice'
        })
        this.say('To speak to a random account executive, press 2',{
            voice:'alice'
        });
        this.say('For customer service, press 3',{
            voice:'alice'
        });
        this.say('To return to the main menu, press 9',{
            voice:'alice'
        })
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/accountExecutiveENAction',function(req, res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if (digit == 9){
        resp.redirect({method:'get'},'/mainMenuEN')
    }
    res.type('xml')
    res.end(resp.toString());
})
route.get('/otherEN',function(req, res){
    var resp = new twilio.TwimlResponse();
    resp.gather({method:'get',action:'otherENAction'},function(){
        this.say('Please leave a message after the tone and someone will follow up with you within 24 business hours.',{
            voice:'alice'
        })
        this.say('To return to the main menu, press 9',{
            voice:'alice'
        });
    })
    res.type('xml')
    res.end(resp.toString());
})
route.get('/otherENAction',function(req, res){
    var digit = req.query.Digits;
    var resp = new twilio.TwimlResponse();
    if (digit == 9){
        resp.redirect({method:'get'},'/mainMenuEN')
    }
    res.type('xml')
    res.end(resp.toString());
})

module.exports = route;