

var accountSid = 'AP7207e6454ae4b23610615d92f4a92643';
var accountToken = 'd3e422a0a43c8d4cbe7e583a0f0443ce';

var twilio = require('twilio'),
    express = require('express'),
    bodyParser = require('body-parser'),
    route_english = require('./routes/english'),
    route_spanish = require('./routes/spanish'),
    route_russian = require('./routes/russian'),
    route_italian = require('./routes/italian');

var app = express();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(route_english);
app.use(route_spanish);
app.use(route_russian);
app.use(route_italian);


app.get('/recieveCall',function(req, res){
    console.log(req.query.Digits)
    var resp = new twilio.TwimlResponse();
    resp.say('Hello and Welcome to KSF');
    resp.gather({timeout:30, method:'get', action:'indexAction'},function(){
        this.say('For english, press 1',{
            voice:'alice',
            language:'en-US'
        })
        this.say("Per l'italiano , premere 2",{
            voice:'alice',
            language:'it-IT'
        })
        this.say('Dlya rossiyskikh , nazhmite 3',{
            voice:'alice',
            language:'ru-RU'
        })

        this.say('Para español , presione 4',{
            voice:'alice',
            language:'es-ES'
        })
    })
    res.type('xml')
    res.end(resp.toString());

})
app.get('/indexAction',function(req, res){
    var resp = new twilio.TwimlResponse();
    var digit = req.query.Digits;
    if(digit == 1){
        resp.redirect({method:'get'},'/mainMenuEN')
    }
    if(digit == 2){
        resp.redirect({method:'get'},'/mainMenuIT')
    }
    if(digit == 3){
        resp.redirect({method:'get'},'/mainMenuRU')
    }
    if(digit == 4){
        resp.redirect({method:'get'},'/mainMenuES')
    }
    res.type('xml')
    res.end(resp.toString())
})

app.get('/recieveSms',function(req, res){
    res.end()
})

app.listen(3020,function(){
    console.log('Server started')
})