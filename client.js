var accountSid = 'AC17d0adef3897ebcc5dbfa6570c2695d2';
var authToken = 'd3e422a0a43c8d4cbe7e583a0f0443ce';
var serverSip = 'AP7207e6454ae4b23610615d92f4a92643';


var twilio = require('twilio'),
    express = require('express');



var app = express();
app.set('view engine','jade')

app.get('/', function(req, res) {

    var capability = new twilio.Capability(
        accountSid,
        authToken
    );

    capability.allowClientOutgoing(serverSip);

    var token = capability.generate();
    res.render('index', {
        token:token
    });
});

app.listen(3050, function(){
    console.log('Client working on server 3050')
});